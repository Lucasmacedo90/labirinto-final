const map = [
    'WWWWWWWWWWWWWWWWWWWWW',
    'W   W     W     W W W',
    'W W W WWW WWWWW W W W',
    'W W W   W     W W   W',
    'W WWWWWWW W WWW W W W',
    'W         W     W W W',
    'W WWW WWWWW WWWWW W W',
    'W W   W   W W     W W',
    'W WWWWW W W W WWW W F',
    'S     W W W W W W WWW',
    'WWWWW W W W W W W W W',
    'W     W W W   W W W W',
    'W WWWWWWW WWWWW W W W',
    'W       W       W   W',
    'WWWWWWWWWWWWWWWWWWWWW'
  ];
 
  
  const jogoLab = document.querySelector('.labirinto');
  const divMainContainer = document.getElementById('maincontainer')
  function printBlocks() {
    for (let i = 0; i < map.length; i++) {
      let linha = document.createElement('div');
      linha.className = 'linha';
  
      for (let j = 0; j < map[i].length; j++) {
        let celula = i + '-' + j;
  
        if (map[i][j] == 'W') {
          console.log("IF = W /i = ", i + " j = ", j);
          let parede = document.createElement('div');
          parede.className = 'parede';
          parede.id = celula;
          linha.appendChild(parede);
        }
  
        if (map[i][j] == ' ') {
          console.log("IF = W /i = ", i + " j = ", j);
          let vazio = document.createElement('div');
          vazio.classList.add('vazio', 'jogoLab');
          vazio.id = celula;
          linha.appendChild(vazio);
        }
  
        if (map[i][j] == 'S') {
          console.log("IF = W /i = ", i + " j = ", j);
          let start = document.createElement('div');
          start.classList.add('start', 'jogoLab');
          start.id = celula;
          linha.appendChild(start);
        }
  
        if (map[i][j] == 'F') {
          console.log("IF = W /i = ", i + " j = ", j);
          let final = document.createElement('div');
          final.classList.add('final', 'jogoLab');
          final.id = celula;
          linha.appendChild(final);
        }
      }
      jogoLab.appendChild(linha);
    }
  }
  printBlocks();
  // até aqui código correto
  let y = 9
let x = 0;
// caractéristicas o jogador criado no CSS
  function criarJogador() {
    let jogador = document.createElement('div');
    jogador.id = 'jogador';
    let celula = y + '-' + x;
    let div = document.getElementById(celula);
    div.appendChild(jogador);
  }
  criarJogador();
  // até aqui correto
  function pegarPosicao() {
    return document.getElementById("jogador").parentElement.id;
  }
  
  function pegarLinha(Posicao) {
    let l = Posicao.split('-');
    console.log(l);
    return parseInt(l[0]);
    
  }
  
  function pegarColuna(Posicao) {
    let c = Posicao.split('-');
    console.log(c);
    return parseInt(c[1]);
  }
  
  function mover(l, c) {
    let id = l + '-' + c;
    document.getElementById(id).appendChild(jogador);
  }
  
  
  
  document.addEventListener('keydown', (event) => {
    const keyName = event.key;
    let posicao = pegarPosicao();
    let l = pegarLinha(posicao);
    let c = pegarColuna(posicao);
  
    if (keyName === "ArrowDown") {
      if (map[l + 1][c] === " ") {
        console.log('entrou no IF')
        mover(l + 1, c);
      }
    }
  
    if (keyName === "ArrowUp") {
      if (map[l - 1][c] === " ") {
        console.log('entrou no IF')
        mover(l - 1, c);
      }
    }
  
    if (keyName === "ArrowLeft") {
      if (map[l][c - 1] === " " || map[l][c - 1] === "S") {
        console.log('entrou no IF')
        mover(l, c - 1);
      }
    }
  
    if (keyName === "ArrowRight") {
      if (map[l][c + 1] === " ") {
        console.log('entrou no IF')
        mover(l, c + 1);
      }
  
      if (map[l][c + 1] === "F") {
        console.log('entrou no IF')
        mover(l, c + 1);
        const divVictory = document.createElement('div')
                    divVictory.id = 'victory'
                    divVictory.innerHTML = `você ganhou!`
                    document.getElementById('alerta2').appendChild(divVictory)
                    //insere fogos de artifício
                    let x = document.createElement('div');
                    x.className = 'meteor';
                    x.id = 'victory'
                    document.getElementById('alerta').appendChild(x)
                    return true
     
                  }
    }
  });
  
  
  
  
  